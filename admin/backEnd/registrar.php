<?php
 require_once("conexion/database.php");
 switch($_POST['option']){

    
    case 'registrar':
        $sql2="select * from tb_usuarios where email_usuario = :correo";
        $stmt2=$conn->prepare($sql2);
        $stmt2-> bindParam(":correo", $_POST['correo'], PDO::PARAM_STR);
        $stmt2->execute();
        if($stmt2->rowCount()>0){
            $json=array(
                'status'=>500,
                'result'=>'Not found'
            );
            echo json_encode($json, http_response_code($json["status"]));
        }else{
            $sql="INSERT INTO tb_usuarios (nombre_usuario,apellidos_usuario,email_usuario,contrasena_usuario,id_rolUsuarios,estado_usuario) VALUES(:nombre,:apellidos,:correo,:contra,2,2)";
            $stmt=$conn->prepare($sql);
            $stmt-> bindParam(":nombre", $_POST['nombre'], PDO::PARAM_STR);
            $stmt-> bindParam(":apellidos", $_POST['apellidos'], PDO::PARAM_STR);
            $stmt-> bindParam(":correo", $_POST['correo'], PDO::PARAM_STR);
            $stmt-> bindParam(":contra", $_POST['contra'], PDO::PARAM_STR);
            if($stmt->execute()){
                $json=array(
                    'status'=>200,
                    'result'=>'success'
                );
                echo json_encode($json, http_response_code($json["status"]));
            }else{
                $json=array(
                    'status'=>500,
                    'result'=>'Not found'
                );
            
                echo json_encode($json, http_response_code($json["status"]));
            }
            $stmt->closeCursor();
            $stmt = null;
        }
    break;
}