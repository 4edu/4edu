<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <meta name="description" content="Proyecto">
    <meta name="Author" content="PrograMaster.Inc">
    <meta name="keywords" content="Proyecto,programaster">    
    <link rel="stylesheet" href="css/estilos-carga.css">
    <title>4Edu</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>
<body id="body">
    <!--<section><img src="../img/loading-9.gif" alt="carga" class="imagen"></section>-->
    <div id="loader"></div><br><br>

    <div id="mensaje" class="text-primary text-center">
        <h1 class="fw-bolder">Cerrando sesión...</h1> 
    </div>
</body>
</html>

<?php
session_start();

session_destroy();
$_SESSION= array();

?>
<br>
<?php header("refresh:1; url=../index.html"); ?> 
   <!-- Bootstrap JS -->

   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>


</body>
</html>