<?php   
 ob_start(); 
 session_start();


    if ($_SESSION['acceso'] != "ok" && $_SESSION['rol'] != 2) {
        
        header("Location:../login.html");
    }
    $_SESSION['url']='https://www.stack-ai.com/embed/2c81fd8b-efc2-4f09-9a20-5af1282669ca/1f14667d-f8cd-496f-a6a1-ddcf315c7bdd/653b2a7700bc51c1695c54d3';
    $_SESSION['targetas']='https://www.stack-inference.com/run_deployed_flow?flow_id=653b2a7700bc51c1695c54d3&org=2c81fd8b-efc2-4f09-9a20-5af1282669ca';

?>
<script>
    var urlIA='<?php echo $_SESSION['targetas']?>';
</script> 
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>4Edu - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="js/menu.js"></script>
    <script src="js/dasboard.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include "nav_left.php";?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
        <?php include "topbar.php";?>
               
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                        
                    </div>

                        <!-- tarjeta -->
                        <div class="col-xl-12 col-lg-7 d-sm-flex">
                            <div class="card shadow mb-1">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Bienvenido</h6>
                                        
                                </div>
                                <!-- Card Body -->
                                <div class="card-body row">
                                        <div class="col-xl-3 col-md-12"><img src="img/001.jpeg" class="img-fluid"></div>
                                        <div class="col-xl-9 col-md-12">
                                        <p class="text-justify">
                                        ¡Hola! Bienvenido a 4Edu. Aquí encontrarás una herramienta que te permitirá adquirir conocimientos en tecnología según tus capacidades. Sabemos que el profesor es una pieza clave en el desarrollo competencial de los alumnos y la comunidad, y que requiere formación y reciclaje sobre el uso de la tecnología. Si formamos al docente, este puede transmitirlo al resto de miembros de la comunidad, acabando así con la desigualdad tecnológica.
                                        ¡Gracias por visitarnos!
                                        </p>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- fin tarjeta -->
                        <div class="row" id="divFrases"></div>
                        <div class="row mt-5">
                        <!-- Gráfica -->
                        <div class="col-sm-12 col-md-6">
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">Categorías más populares</h1>
                            </div>
                            <div id="grafica" style="width: 100%; height: 300px; Margin-bottom: 60px;"></div>
                        </div>

                        <!-- Tarjetas de favoritos -->
                        <div class="col-sm-12 col-md-6">
                            <h1 class="h3 mb-0 text-gray-800 mb-3">Categorías favoritas</h1>
                            
                            <div id="idConten" class="row align-items-start"></div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
        <?php include "footer.php";?>

            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Perfil Modal-->
    <div class="modal fade" id="miPerfilModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tu Perfil</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-5 d-flex justify-content-end d-none d-sm-none d-md-block">
                            <img class="img-profile rounded-circle"
                                    src="img/undraw_profile.svg">
                        </div>
                        <div class="col-7 d-flex justify-content-start">
                            <div class="card" style="width: 38rem;">
                                <div class="card-body">
                                  <h5 class="card-title">Información personal</h5>
                                  <h6 class="card-subtitle mb-2 text-muted">Nombre Completo: <bold class="text-black fw-bolder">Irvin Alexis Hernandez Castañeda</bold></h6>
                                  <h6 class="card-subtitle mb-2 text-muted">Correo electrónico: <bold class="text-black fw-bolder">h@gmail.com</bold></h6>
                                  <h6 class="card-subtitle mb-2 text-muted">Contraseña: <bold class="text-black fw-bolder">hh**********</bold></h6>
                                  <hr>
                                  <h5 class="card-title">Información académica o profesional</h5>
                                  <div class="form-group">
                                    <label for="emailInfo">Correo electrónico</label>
                                    <input type="email" class="form-control form-control-user"
                                        id="emailInfo" aria-describedby="emailHelp"
                                        placeholder="Introduce un correo electrónico">
                                    </div>
                                    <div class="form-group">
                                        <label for="emailInfo">Contraseña</label>
                                        <input type="password" class="form-control form-control-user"
                                            id="contraseñaInfo" placeholder="Introduce una contraseña">
                                    </div>
                                  <a href="#" class="card-link">Card link</a>
                                  <a href="#" class="card-link">Another link</a>
                                </div>
                              </div>
                        </div>
                      </div>                      
                </div>
            </div>
        </div>
    </div>
    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Selecciona "Cerrar sesión" a continuación si estás listo para finalizar tu sesión actual.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-primary" href="salir.php">Cerrar sesión</a>
                </div>
            </div>
        </div>
    </div>







    
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>


    
    <div id="jamon" class=" rounded-circle" style="position: fixed; z-index: 9999999999; height: 6%;  bottom: 2%; right:7%; border: none; border-radius: 10px;">
    <img src="img/logo.jpg" class="rounded-circle" width ="50px">    
    <iframe  id="responsiveIframe" class="chatbot-container d-none" src='<?php echo $_SESSION['url']?>' style="position: fixed; z-index: 9999999999; height: 80%;  bottom: 1%; right:6.5%; border: none; border-radius: 10px;" />
    </div>

</body>

</html>