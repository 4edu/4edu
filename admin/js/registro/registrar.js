function registrar() {
    var nombre = document.getElementById("input-nombre").value;
    var apellidos = document.getElementById("input-apellidos").value;
    var correo = document.getElementById("input-correo").value;
    var contra = document.getElementById("input-contra").value;
    var contraRep = document.getElementById("input-contraRep").value;
    var option = "registrar";
  
    var errorNombre = document.getElementById("error-nombre");
    errorNombre.textContent = "";
  
    var errorApellido = document.getElementById("error-apellido");
    errorApellido.textContent = "";
  
    var errorCorreo = document.getElementById("error-correo");
    errorCorreo.textContent = "";
  
    var errorContra = document.getElementById("error-contra");
    errorContra.textContent = "";
  
    var errorContraRep = document.getElementById("error-contraRep");
    errorContraRep.textContent = "";

    
    if (nombre == "") {
      errorNombre.textContent = "El nombre esta vacio";
    } 
    if (apellidos == "") {
      errorApellido.textContent = "Los apellidos estan vacio";
    }
    if (!valCorreo(correo)) {
      errorCorreo.textContent = "El correo no es válido.";
    }
    if (!valpas(contra)) {
      errorContra.textContent =
        "La contraseña debe tener al menos 8 caracteres con Mayúsculas, Minúsculas, Números y Caracteres Especiales.";
    }
    if (!valpas2(contra, contraRep)) {
      errorContraRep.textContent = "Las contraseñas no coinciden.";
    } 
    
    if(nombre != "" && apellidos != "" && valCorreo(correo) && valpas(contra) && valpas2(contra, contraRep)){

      const fData = new FormData();
      fData.append("nombre", nombre);
      fData.append("apellidos", apellidos);
      fData.append("correo", correo);
      fData.append("contra", contra);
      fData.append("option", option);
    
      fetch("backEnd/registrar.php", {
        method: "POST",
        body: fData,
      }) //hasta aqui ok
        .then(function (response) {
          if (response.ok) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              })
              
              Toast.fire({
                icon: 'success',
                title: 'Se registro correctamente el usuario'
              });

              var inputNombre = document.getElementById("input-nombre");
              inputNombre.value = "";

              var inputApellido = document.getElementById("input-apellidos");
              inputApellido.value = "";

              var inputCorreo = document.getElementById("input-correo");
              inputCorreo.value = "";

              var inputContra = document.getElementById("input-contra");
              inputContra.value = "";

              var inputContraRep = document.getElementById("input-contraRep");
              inputContraRep.value = "";

              setTimeout(function() {
                window.location.href = "login.html";
              }, 3000);
              
          } else {
            errorCorreo.textContent = "El correo ya existe.";
          }
        });
    }
    
  }
  
  function valCorreo(correo) {
    if (!tieneArroba(correo)) {
      return false;
    } else {
      return true;
    }
  }
  function valpas(pas) {
    if (
      !tieneNumeros(pas) ||
      !tieneMinusculas(pas) ||
      !tieneMayusculas(pas) ||
      !tieneCarEsp(pas) ||
      pas.length < 8
    ) {
      return false;
    } else {
      return true;
    }
  }
  function valpas2(pas, pas2) {
    if (pas2 == pas) {
      return true;
    } else {
      return false;
    }
  }
  function tieneNumeros(cadena) {
    numeros = "0123456789";
  
    for (var i = 0; i < cadena.length; i++) {
      c = cadena.charAt(i);
      if (numeros.indexOf(c) != -1) {
        return true;
      }
    }
    return false;
  }
  function tieneMinusculas(cadena) {
    minusculas = "abcdefghijklmnñopqrstuvwxyz";
    for (var i = 0; i < cadena.length; i++) {
      c = cadena.charAt(i);
      if (minusculas.indexOf(c) != -1) {
        return true;
      }
    }
    return false;
  }
  function tieneMayusculas(cadena) {
    minusculas = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    for (var i = 0; i < cadena.length; i++) {
        c = cadena.charAt(i);
        if (minusculas.indexOf(c) != -1) {
            return true;
        }
    }
    return false;
  }
  function tieneCarEsp(cadena) {
    minusculas = ",;.:-_{}[]´+¨*'¿?¡°!#$%&/()=@^`~";
    for (var i = 0; i < cadena.length; i++) {
      c = cadena.charAt(i);
      if (minusculas.indexOf(c) != -1) {
        return true;
      }
    }
    return false;
  }
  function tieneArroba(correo) {
    arroba = "@";
  
    for (var i = 0; i < correo.length; i++) {
      c = correo.charAt(i);
      if (arroba.indexOf(c) != -1) {
        return true;
      }
    }
    return false;
  }
