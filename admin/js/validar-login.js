// Obtén el botón, el formulario y el elemento span que quieres usar para mostrar el mensaje de error
var correo = document.getElementById('email');
var passwd = document.getElementById('password');

function iniciar() {
    var correo = document.getElementById('email').value;
    var passwd = document.getElementById('password').value;

    if (correo === '') {
        document.getElementById("a-correo").style.display = "block";
        c = false;
    } else {
        document.getElementById("a-correo").style.display = "none";
        c = true;
    }

    if (passwd === '') {
        document.getElementById("a-passwd").style.display = "block";
        p = false;
    } else {
        document.getElementById("a-passwd").style.display = "none";
        p = true;
    }

    if (c == true && p == true) {
        $.ajax({
            url: 'login.php',
            type: 'POST',
            data: {
                email: correo,
                password: passwd
            },
            beforeSend: function () {
                Swal.fire({
                    title: 'Iniciando sesión...',
                    showConfirmButton: false,
                    customClass: { title: "text-success fw-bolder", footer: "fw-bolder" },
                    html: '<div id="body"><div id="loader" class="m-5"></div></div>',
                    footer: 'Evite recargar la página',
                    allowOutsideClick: false,
                    allowEscapeClick: false,
                    allowEnterClick: false,
                    stopKeydownPropagation: true,
                });
            }
        }).done(function (resp) {
            if (resp == "error") {
                Swal.fire({
                    title: "Hubo un error",
                    text: "Usuario y/o contraseña incorrectos!!",
                    icon: "error",
                    confirmButtonColor: "#0d6efd",
                    confirmButtonText: "Aceptar"
                });
            }
            else {
                window.location = 'index.php';
            }
        })
    }
};
