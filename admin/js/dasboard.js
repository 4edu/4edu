document.addEventListener("DOMContentLoaded", function () {
    datosGrafica();
    favorito();
});

function grafica(etiquetas, valores) {

    // Datos para tu gráfica
    const labels = etiquetas;
    const data = {
        labels: labels,
        datasets: [{
            label: 'Más populares',
            data: valores,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 205, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(201, 203, 207, 0.2)',
                'rgba(0, 255, 255, 0.2)'
            ],
            borderColor: [
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)',
                'rgb(0, 255, 255)'
            ],
            borderWidth: 1
        }]
    };
    const config = {
        type: 'bar',
        data: data,
        options: {
            responsive: true, // Permite que la gráfica sea responsiva
            maintainAspectRatio: false, 
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

    // Crear un elemento canvas en el DOM para la gráfica
    var canvas = document.createElement('canvas');
    canvas.id = 'myChart';
    document.getElementById('grafica').appendChild(canvas);

    // Inicializar la gráfica
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, config);
}

function datosGrafica(){
    $.ajax({
        data: 'option=ranking',
        url: 'control/categorias.php',
        type: 'POST',
        success: function (datos) {
            var data = JSON.parse(datos);

            // Inicializa los arrays para etiquetas y valores
            var labels = [];
            var values = [];

            // Procesa los datos y almacena en los arrays
            data.forEach(function (post) {
                labels.push(post.nombre_post);
                values.push(post.ranking);
            });

            grafica(labels, values);

        }
    })
}


function favorito() {
    $.ajax({
        data: 'option=favorito',
        url: 'control/post.php',
        type: 'POST',
        success: function (datos) {
            $('#idConten').html(datos);
        }
    });
}

function contenido(id) {
   

    $.ajax({
        data: {option:"post",id:id},
        url: 'control/post.php',
        type: 'POST',
        success: function (datos) {
            $('#idConten').html(datos);
            
        }
    });
}