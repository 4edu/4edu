<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy;4edu 2023 by <a href="https://github.com/CDS-UTSEM">DSUT</a></span>
        </div>
    </div>
    
</footer>

